<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\base\InvoiceBase */

$this->title = 'Создать накладную';
$this->params['breadcrumbs'][] = ['label' => 'Invoice Bases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-base-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Invoice */

$this->title = 'Накладные';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-base-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        \yii\bootstrap\Modal::begin([
            'header' => '<h2>Создать накладную</h2>',
            'toggleButton' => [
                'label' => 'Создать накладную',
                'tag' => 'a',
                'class' => 'btn btn-success',
            ],
        ]);
        echo $this->render('_form', [
            'model' => $model,
        ]);
        \yii\bootstrap\Modal::end();
        ?>
    </p>

    <?php
    \yii\bootstrap\Modal::begin([
        'header' => '<h2>Изменить накладную</h2>',
        'id' => 'modal',
    ]);
    echo "<div id='modalContent'></div>";
    \yii\bootstrap\Modal::end();
    ?>

    <?php
    $this->registerJs("
        $(function () {
            $('body').on('click', 'button.btn-update', function () {
             var container = $('#modalContent');
      
             container.html('Please wait, the data is being loading...');
                $('#modal').modal('show')
                .find('#modalContent')
                .load($(this).attr('value'));
            });
        });
    ")
    ?>

    <?php \yii\widgets\Pjax::begin(['id' => 'invoices']) ?>
    <?= GridView::widget(['dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'city_from',
            'city_to',
            'recipient',
            [
                'attribute' => 'status',
                'value' => 'statusLabel',
                'filter' => \common\models\Invoice::getStatusLabels()
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => ['update' => function ($url, $dataProvider, $key) {
                    return Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['value' => \yii\helpers\Url::to(['invoice/update', 'id' => $dataProvider->id]),
                        'class' => 'btn-update',
                        'data-pjax' => '0',]);
                },
                ],
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>

<?php

/* @var $this yii\web\View */
/* @var $model common\models\base\InvoiceBase */

echo $this->render('_form', [
    'model' => $model,
]);
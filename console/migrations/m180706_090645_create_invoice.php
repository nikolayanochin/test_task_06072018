<?php

use yii\db\Migration;

/**
 * Class m180706_090645_create_invoice
 */
class m180706_090645_create_invoice extends Migration
{
    const INVOICE = '{{%invoice}}';
//    const CITY = '{{%city}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::INVOICE, [
            'id' => $this->primaryKey()->comment('Идентификатор'),
            'city_from' => $this->string(255)->notNull()->comment('Откуда'),
            'city_to' => $this->string(255)->notNull()->comment('Куда'),
            'recipient' => $this->string(255)->notNull()->comment('Получатель'),
            'status' => $this->smallInteger(2)->notNull()->defaultValue(0)->comment('Статус'),
            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата редактирования'),
        ]);

//        $this->addForeignKey('invoice_city_from__city_id', self::INVOICE, 'city_from', self::CITY, 'id');
//        $this->addForeignKey('invoice_city_to__city_id', self::INVOICE, 'city_to', self::CITY, 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->dropForeignKey('invoice_city_from__city_id', self::INVOICE);
//        $this->dropForeignKey('invoice_city_to__city_id', self::INVOICE);

        $this->dropTable(self::INVOICE);
    }

}

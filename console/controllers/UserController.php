<?php
/**
 * Created with love by Альянс Экспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 06.07.2018
 * Time: 12:23
 */

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\VarDumper;
use common\models\User;
use console\models\SignupForm;


/**
 * Можно создать пользователя и изменить ему пароль.
 * Контроллер нужен для инициализации админа.
 *
 */
class UserController extends Controller
{
    public function actionCreate($username, $email, $password)
    {
        $model = new User();

        $model->username = $username;
        $model->email = $email;
        $model->password = $password;

        if ($model->save(false)) {
            $this->stdout("User created: {$username}/{$password}, don't forget about role field ;)\r\n");
            return 0;
        } else {
            VarDumper::dump($model->getErrors());
            return 1;
        }
    }

    public function actionCreateDefault()
    {
        $this->actionCreate('admin', 'admin@example.com', 'admin1');
    }

    public function actionChangePassword($username, $password)
    {
        /** @var User $model */
        $model = User::findByUsername($username);
        if ($model == null) {
            echo "Can`t find user\n";
            return 1;
        }

        $model->setPassword($password);

        if (!$model->save()) {
            echo "Can`t set password\n";
            return 1;
        }
        echo "Success\nYt gj";
        return 0;
    }
} 
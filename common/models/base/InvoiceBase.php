<?php

namespace common\models\base;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id Идентификатор
 * @property string $city_from Откуда
 * @property string $city_to Куда
 * @property string $recipient Получатель
 * @property int $status Статус
 * @property int $created_at Дата создания
 * @property int $updated_at Дата редактирования
 */
class InvoiceBase extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_from', 'city_to', 'recipient'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['city_from', 'city_to', 'recipient'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'city_from' => 'Откуда',
            'city_to' => 'Куда',
            'recipient' => 'Получатель',
            'status' => 'Статус',
            'statusLabel' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }
}

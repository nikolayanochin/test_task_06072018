<?php
/**
 * Created with love by Альянс Экспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 06.07.2018
 * Time: 12:34
 */


namespace common\models;

use common\models\base\InvoiceBase;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Invoice
 * @package common\models
 */
class Invoice extends InvoiceBase
{
    const STATUS_PENDING = 0;
    const STATUS_DELIVERED = 1;
    const STATUS_ON_WAY = 2;
    const STATUS_TAKEN_TO_WAREHOUSE = 3;
    const STATUS_RETURNED = 4;

    /**
     * Статус накладной
     * @return mixed
     */
    public function getStatusLabel()
    {
        return ArrayHelper::getValue(self::getStatusLabels(), $this->status);
    }

    /**
     * Список статусов накладных
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_PENDING => 'Ожидает отправки',
            self::STATUS_DELIVERED => 'Доставлено',
            self::STATUS_ON_WAY => 'В пути',
            self::STATUS_TAKEN_TO_WAREHOUSE => 'Принято на склад',
            self::STATUS_RETURNED => 'Возвращен'
        ];
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }
}